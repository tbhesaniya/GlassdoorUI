﻿var loginID = 0;
var app = angular.module('myApp', ['ngRoute', 'chart.js']);
//var app = angular.module('chartsapp', ['chart.js']);
//var app = angular.module('myApp', ['chart.js']);
app.config(function ($routeProvider, $locationProvider) {
    try {
        $locationProvider.hashPrefix('');
        //if ($window.sessionStorage.getItem("CompanyID") != null)
        {
            $routeProvider

                .when('/dashboard', {
                    templateUrl: 'dashboard.html',
                    controller: 'HomeController'
                })

                .when('/jobs', {
                    templateUrl: 'Jobs.html',
                    controller: 'HomeController'
                })

                .when('/createJob', {
                    templateUrl: 'createJob.html',
                    controller: 'HomeController'
                })

                .when('/Companies', {
                    templateUrl: 'companies.html',
                    controller: 'HomeController'
                })

                .when('/EditCompany', {
                    templateUrl: 'editCompany.html',
                    controller: 'HomeController'
                })

                .when('/charts', {
                    templateUrl: 'charts.html',
                    controller: 'HomeController'
                })

                .when('/reviews', {
                    templateUrl: 'reviews.html',
                    controller: 'HomeController'
                })

                .when('/recruitment', {
                    templateUrl: 'Jobrecruitment.html',
                    controller: 'HomeController'
                })

                .when("/info", {
                    templateUrl: "Views/companyInfo.html",
                    controller: 'infoController'
                })
                .when("/jobs", {
                    templateUrl: "Jobs.html",
                    controller: 'HomeController'
                })

                .when("/reviews2", {
                    templateUrl: "Views/companyReviews.html",
                    controller: 'reviewController'
                })

                .when("/jobs2", {
                    templateUrl: "Views/CompanyJobs.html",
                    controller: 'jobController'
                })

                .when("/dilshod", {
                    templateUrl: "dd.html",
                    controller: 'jobController'
                })


                .otherwise({ redirectTo: '/dashboard' });
        }

    }
    catch (err) {
        alert(err);
    }

});

app.controller("infoController", function ($scope, $http, $routeParams, $window) {
    console.log("info controller");
    $http.get("http://localhost:59424/dashboard/Cpydashboard/GetCompanyInfo", { params: { "companyID": $window.sessionStorage.getItem("CompanyID") } }).then(function (res) {
        $scope.companyInfo = res.data;
        console.log($scope.companyInfo);
    })

});

app.controller("jobController", function ($scope, $http, $routeParams, $window) {
    console.log("job controller");
    $http.get("http://localhost:59424/dashboard/Cpydashboard/GetJobs", { params: { "companyID": $window.sessionStorage.getItem("CompanyID") } }).then(function (res) {
        $scope.companyJobs = res.data;
        console.log($scope.companyJobs);
    })
});

app.controller("reviewController", function ($scope, $http, $routeParams, $window) {
    console.log("review controller");
    $http.get("http://localhost:59424/dashboard/Cpydashboard/GetReviewsAndReplies", { params: { "companyID": $window.sessionStorage.getItem("CompanyID") } }).then(function (res) {
        $scope.companyReviews = res.data;
        console.log($scope.companyReviews);
    })
});

app.controller("jobDetailController", function ($scope, $routeParams) {
    $scope.JobID = $routeParams.JobID;
});

app.controller('HomeController', function ($scope, $window, $rootScope) {
    $scope.message = 'Hello from HomeController';

    $rootScope.CompanyNameX = $window.sessionStorage.getItem("CompanyName");
});

app.directive("menuDir", function () {

    var dir = {};
    dir.templateUrl = 'views/menubar.html'
    return dir;
}
);

app.directive("menuDir2", function () {

    var dir = {};
    dir.templateUrl = 'menubar.html'
    return dir;
}
);

app.directive("footerDir", function () {

    var dir = {};
    dir.templateUrl = 'views/footer.html'
    return dir;
}
);


app.service("httpFactoryAll", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Companies");
    }

});



app.controller('ListCompanyController', function ($scope, $rootScope, $http, httpFactoryAll, httpFactoryCompanyID) {
    httpFactoryAll.getData().then(function (data) {

        $scope.company = data.data;
        console.log($scope.company);

    })

    $scope.viewCompany = function (id) {
        httpFactoryCompanyID.redditGet(id)
            .then(function (response) {
                $rootScope.company = response;

                $rootScope.CompanyIDx = response.data.CompanyID;
                $rootScope.CompanyName = response.data[0].CompanyName;
                $rootScope.Address = response.data[0].Address;
                $rootScope.City = response.data[0].City;
                $rootScope.State = response.data[0].State;
                $rootScope.Country = response.data[0].Country;

                $rootScope.Phone = response.data[0].Phone;
                $rootScope.Website = response.data[0].Website;
                $rootScope.CompanyType = response.data[0].CompanyType;
                $rootScope.CompanySector = response.data[0].CompanySector;
                $rootScope.CompanyContactName = response.data[0].CompanyContactName;
                $rootScope.Email = response.data[0].Email;



                console.log($rootScope.company);





            })
        response;
    };

});

app.factory('httpFactoryCompanyID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/Companies/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/Companies/')
        }

    }
});


app.controller('ListJobsController', function ($scope, $rootScope, $http, httpFactoryJobID, httpFactoryJobCompanyID, $window, httpFactoryDeleteJobID, httpStateID, httpGetlistcity, httpGetliststate, httpFactoryAllJobsRaju) {
    /*httpFactoryAllCompanyJobs.getData().then(function (data) {
        $scope.jobs = data.data;
        console.log($scope.jobs);

        id = data.data.CountryID;

        z = 0
        httpStateID.redditGet(id)
            .then(function (dataS) {
                $rootScope.state = dataS.data;
                console.log($scope.state);
            })

        //$scope.jobs.StateID = data.data[z].StateID; 
    })*/

    httpFactoryAllJobsRaju.getData().then(function (data) {
        $scope.jobs = data.data;
        console.log($scope.jobs);

    })


    idx = $window.sessionStorage.getItem("CompanyID");
    httpFactoryJobCompanyID.redditGet(idx)
        .then(function (response) {
            $rootScope.jobsX = response.data;
            console.log($rootScope.jobsX);
        })

    $scope.viewJob = function (id) {

        httpFactoryJobID.redditGet(id)
            .then(function (response) {
                $rootScope.job = response;

                $rootScope.JobID = response.data[0].JobID;
                $rootScope.JobName = response.data[0].JobName;
                $rootScope.JobDescription = response.data[0].JobDescription;
                $rootScope.Company = response.data[0].Company;
                $rootScope.DateCreated2 = response.data[0].DateCreated2;
                $rootScope.Address = response.data[0].Address;
                $rootScope.City = response.data[0].City;
                $rootScope.State = response.data[0].State;
                $rootScope.StateID = response.data[0].StateID;
                $rootScope.Country = response.data[0].Country;
                $rootScope.CountryID = response.data[0].CountryID;
                $rootScope.vDaysSponsered = response.data[0].DaysSponsered;

                $rootScope.cl = response.data[0].Clicks + 1;

                console.log($rootScope.job);
                idC = $window.sessionStorage.getItem("CompanyID");
                //idx = $window.sessionStorage.getItem("CompanyID");
                $http.put('http://localhost:59424/api/djob/' + id,
                    {
                        JobID: id,
                        JobName: $rootScope.JobName,
                        JobDescription: $rootScope.JobDescription,
                        CompanyID: idC,
                        DateCreated: $rootScope.DateCreated2,
                        Address: $rootScope.Address,
                        City: $rootScope.City,
                        StateID: $rootScope.StateID,
                        CountryID: $rootScope.CountryID,
                        DaysSponsered: $rootScope.vDaysSponsered,
                        Clicks: $rootScope.cl
                    })


            })

        idx = $window.sessionStorage.getItem("CompanyID");
        httpFactoryJobCompanyID.redditGet(idx)
            .then(function (response) {
                $rootScope.jobsX = response.data;
                console.log($rootScope.jobsX);

                //$scope.jobsX = response.data;
                //console.log($scope.jobs);
                /*$scope.objects = $rootScope.jobsX;
                $scope.totalItems = $rootScope.jobsX.length;
                $scope.currentPage = 1;
                $scope.numPerPage = 5;

                $scope.paginate = function (value) {
                    var begin, end, index;
                    begin = ($scope.currentPage - 1) * $scope.numPerPage;
                    end = begin + $scope.numPerPage;
                    index = $scope.objects.indexOf(value);
                    return (begin <= index && index < end);
                };*/

            })
    };



    $scope.deleteJob = function (id, name) {

        if ($window.confirm("Please confirm delete Job: " + name)) {
            $scope.status = "You clicked YES.";

            httpFactoryDeleteJobID.redditGet(id)
                .then(function (response) {
                    $scope.message = 'Deleted Job! Refreshing jobs list.';
                    for (var i = 0; i < $scope.jobsX.length; i++) {
                        var t = $scope.jobsX[i];
                        if (t.JobID === id) {
                            $scope.jobsX.splice(i, 1);
                            break;
                        }
                    }

                }, function (error) {
                    $rootScope.message = 'Unable to delete job: ' + error.message;
                });
        } else {
            $scope.Message = "You clicked NO.";
        }

    };

    $scope.updateJob = function (index) {
        if ($scope.editing !== false) {
            $scope.jobsX[$scope.editing] = $scope.newField;
            $scope.editing = false;
            $scope.selectedRow = index;
            $scope.jobsX[index].JobName;
            idX = $window.sessionStorage.getItem("CompanyID");
            $http.put('http://localhost:59424/api/Jobs/' + $scope.jobsX[index].JobID,
                {
                    JobID: $scope.jobsX[index].JobID,
                    CompanyID: idX,
                    JobName: $scope.jobsX[index].JobName,
                    JobDescription: $scope.jobsX[index].JobDescription,
                    City: $scope.jobsX[index].City,
                    CountryID: $scope.jobsX[index].CountryID,
                    StateID: $scope.jobsX[index].StateID,
                    DateCreated: $scope.jobsX[index].DateCreated2,
                    Address: $scope.jobsX[index].Address,
                    DaysSponsered: $scope.jobsX[index].DaysSponsered,
                    Clicks: $scope.jobsX[index].Clicks
                });
        }
    };

    httpGetlistcity.getData().then(function (d) {

        $rootScope.op = d.data;
        console.log($scope.op.length);
        console.log($scope.op);
    })

    httpGetliststate.getData().then(function (e) {
        $rootScope.stlist = e.data;
        console.log($scope.stlist.length);
        console.log($scope.stlist);
    })
    $scope.search = function (item) {
        if ($scope.selected == undefined) {
            return true;
        }
        else {
            if (item.City.toLowerCase().indexOf($scope.selected.toLowerCase()) != -1 ||
                item.State.toLowerCase().indexOf($scope.selected1.toLowerCase()) != -1) {
                return true;
            }
        }
        return false;
    }
    //$scope.clear = function ($scope) {
    //    debugger
    //    //app.controller.ListJobsController;

    //    FindJob.value = "";
    //    city.value = 0;
    //    state.value = 0;
    //    //$("#FindJob").value = "";
    //    //$("#city").selected = 0;
    //    //$("#state").selected = 0;
    //    //$scope.City.value = 0;
    //    //$scope.State.value = 0;
    //}


    GetAll();
    function GetAll() {
        $http.get('http://localhost:59424/api/Jobs').then(function (response) { $scope.RMS = response.data }, function () { alert("Failed To Get") })

    }

    $scope.set_color = function (expdate) {
        //if (exp <= "2018-05-27T00:00:00") {
        if (expdate <= "2018-06-29T00:00:00") {
            return { color: 'red' }
        }
    }

});

app.service("httpFactoryAllJobsRaju", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Jobs");
    }

});


app.service("httpGetlistcity", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Jobs/Getlistcity");
    }
});

app.service("httpGetliststate", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Jobs/Getliststate");
    }
});


app.service("httpFactoryAllJobs", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Jobs");
    }

});

app.service("httpFactoryAllCompanyJobs", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/CompanyJobs");
    }

});


app.factory('httpFactoryJobID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/Jobs/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/Jobs/')
        }

    }
});

app.factory('httpFactoryJobCompanyID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/JobsCompany/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/JobsCompany/')
        }

    }
});

app.factory('httpFactoryDeleteJobID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(id) {
        return $http.delete('http://localhost:59424/api/Jobs/' + id);
    };
});

app.factory('httpFactoryDeleteReviewID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(id) {
        return $http.delete('http://localhost:59424/api/JobsReviews/' + id);
    };
});

app.factory('httpStateID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/States/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/States/')
        }

    }
});

app.controller('createJob', function ($scope, $rootScope, $http, $window) {
    $scope.submit = function (isValid) {

        $scope.url = 'http://localhost:59424/api/Jobs/';
        if (isValid) {

            try {
                id = $window.sessionStorage.getItem("CompanyID");
                var res = $http.post('http://localhost:59424/api/Jobs/',
                    {
                        JobName: $scope.job.JobName,
                        JobDescription: $scope.job.JobDescription,
                        CompanyID: id,
                        DateCreated: new Date(),
                        Address: $scope.job.JobAddress,
                        City: $scope.job.City,
                        StateID: $scope.job.SelectedState,
                        CountryID: $scope.job.SelectedCountry,
                        DaysSponsered: $scope.job.DaysSponsored
                    });

                //res.success(function (data, status, headers, config) {
                alert("Successfully Updated ");

                //Clear Form

                $scope.job.JobName = "";
                $scope.job.JobDescription = "";
                $scope.job.JobAddress = "";
                $scope.job.City = "";
                $scope.job.SelectedState = -1,
                    $scope.job.SelectedCountry = -1,
                    $scope.job.DaysSponsored = "";


                //$scope.message = data;
                /*});
                res.error(function (data, status, headers, config) {
                    alert("failure message: " + JSON.stringify({ data: data }));
                });*/
            }

            catch (err) {
                alert("Error....." + err);
                console.log(err);
            }
        }

        else {

            alert('Form is not valid');
        }
    };
});

app.controller('ListCountriesController', function ($scope, $rootScope, $http, httpCountries, httpStateID) {

    httpCountries.getData().then(function (data) {

        $scope.country = data.data;
        console.log($scope.country);

    })

    $scope.viewState = function (id) {

        httpStateID.redditGet(id)
            .then(function (data) {
                $rootScope.state = data.data;
                console.log($scope.state);

            })
    }
});

app.service("httpCountries", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Countries");
    }

});

app.controller('CompanyDataController', function ($scope, $rootScope, $http, httpCountries, httpFactoryCompanyID, httpStateID, httpSectorID, $window) {

    httpCountries.getData().then(function (data) {
        id = $window.sessionStorage.getItem("CompanyID");
        httpFactoryCompanyID.redditGet(id)
            .then(function (data) {
                $rootScope.company = data.data;
                console.log($scope.company);
                $rootScope.company.CompanyID = data.data[0].CompanyID;
                $rootScope.company.CompanyName = data.data[0].CompanyName;
                $rootScope.company.Address = data.data[0].Address;
                $rootScope.company.City = data.data[0].City;
                $rootScope.company.Phone = data.data[0].Phone;
                $rootScope.company.Website = data.data[0].Website;
                $rootScope.company.CompanyContactName = data.data[0].CompanyContactName;
                $rootScope.company.Username = data.data[0].Username;
                $rootScope.company.Password = data.data[0].Password;
                $rootScope.company.Email = data.data[0].Email;
                $rootScope.company.SelectedCountry = data.data[0].CountryID;
                $rootScope.company.CompanyDescription = data.data[0].Description;
                $rootScope.company.SelectedCompanyType = data.data[0].CompanyTypeID;
                $rootScope.company.SelectedStatusType = data.data[0].CompanyStatusID;

                id = data.data[0].CountryID;
                id2 = data.data[0].CompanyTypeID;

                if (id != null) {
                    httpStateID.redditGet(id)
                        .then(function (data2) {
                            $rootScope.state = data2.data;
                            console.log($scope.state);
                        })

                    $rootScope.company.SelectedState = data.data[0].StateID;
                }

                if (id2 != null) {
                    httpSectorID.redditGet(id2)
                        .then(function (data3) {
                            $rootScope.sector = data3.data;
                            console.log($scope.sector);

                        })

                    $rootScope.company.SelectedSector = data.data[0].CompanySectorID;
                }

            })
    })
});

app.factory('httpSectorID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/CompanySectors/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/CompanySectors/')
        }

    }
});

app.controller('updateCompany', function ($scope, $rootScope, $http, $window) {
    $scope.submit = function (isValid) {

        $scope.url = 'http://localhost:59424/api/Companies/';
        if (isValid) {

            try {


                $rootScope.company.CompanyName;
                $window.sessionStorage.getItem("img");
                var res = $http.put('http://localhost:59424/api/Companies/' + $rootScope.company.CompanyID,
                    {
                        CompanyID: $rootScope.company.CompanyID,
                        CompanyName: $rootScope.company.CompanyName,
                        Address: $rootScope.company.Address,
                        City: $rootScope.company.City,
                        Phone: $rootScope.company.Phone,
                        Website: $rootScope.company.Website,
                        CompanyContactName: $rootScope.company.CompanyContactName,
                        Username: $rootScope.company.Username,
                        Password: $rootScope.company.Password,
                        Email: $rootScope.company.Email,
                        CountryID: $rootScope.company.SelectedCountry,
                        Description: $rootScope.company.CompanyDescription,
                        CompanyTypeID: $rootScope.company.SelectedCompanyType,
                        CompanyStatusID: $rootScope.company.SelectedStatusType,
                        StateID: $rootScope.company.SelectedState,
                        CompanySectorID: $rootScope.company.SelectedSector
                        //Image: $window.sessionStorage.getItem("img")
                    });

                //res.success(function (data, status, headers, config) {
                alert("Successfully Updated ");
                //$scope.message = data;
                /*});
                res.error(function (data, status, headers, config) {
                    alert("failure message: " + JSON.stringify({ data: data }));
                });*/
            }

            catch (err) {
                alert("Error....." + err);
                console.log(err);
            }
        }

        else {

            alert('Form is not valid');
        }
    };


});

app.controller('ListCompanyTypesController', function ($scope, $rootScope, $http, httpCompanyTypes, httpSectorID) {
    httpCompanyTypes.getData().then(function (data) {
        $scope.ct = data.data;
        console.log($scope.ct);

    })

    $scope.viewCompanySector = function (id) {
        httpSectorID.redditGet(id)
            .then(function (data) {
                $rootScope.sector = data.data;
                console.log($scope.sector);

            })
    }

});

app.service("httpCompanyTypes", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/CompanyTypes");
    }

});

app.controller('ListCompanyStatusController', function ($scope, $rootScope, $http, httpCompanyStatus) {
    httpCompanyStatus.getData().then(function (data) {
        $scope.st = data.data;
        console.log($scope.st);

    })
});

app.service("httpCompanyStatus", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/CompanyStatus");
    }

});

app.controller('UploadController', function ($scope, fileReader) {
    $scope.imageSrc = "";

    $scope.$on("fileProgress", function (e, progress) {
        $scope.progress = progress.loaded / progress.total;
    });
});

app.directive("ngFileSelect", function (fileReader, $timeout) {
    return {
        scope: {
            ngModel: '='
        },
        link: function ($scope, el) {
            function getFile(file) {
                fileReader.readAsDataUrl(file, $scope)
                    .then(function (result) {
                        $timeout(function () {
                            $scope.ngModel = result;
                        });
                    });
            }

            el.bind("change", function (e) {
                var file = (e.srcElement || e.target).files[0];
                getFile(file);
            });
        }
    };
});

app.factory("fileReader", function ($q, $log) {
    var onLoad = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.resolve(reader.result);
            });
        };
    };

    var onError = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.reject(reader.result);
            });
        };
    };

    var onProgress = function (reader, scope) {
        return function (event) {
            scope.$broadcast("fileProgress", {
                total: event.total,
                loaded: event.loaded
            });
        };
    };

    var getReader = function (deferred, scope) {
        var reader = new FileReader();
        reader.onload = onLoad(reader, deferred, scope);
        reader.onerror = onError(reader, deferred, scope);
        reader.onprogress = onProgress(reader, scope);
        return reader;
    };

    var readAsDataURL = function (file, scope) {
        var deferred = $q.defer();

        var reader = getReader(deferred, scope);
        reader.readAsDataURL(file);

        return deferred.promise;
    };

    return {
        readAsDataUrl: readAsDataURL
    };
});


app.controller('ListJobsReviewsController', function ($scope, $rootScope, $http, httpFactoryAllJobsCompanyReviews, $window, httpFactoryJobReviewID, httpFactoryDeleteReviewID) {
    /*httpFactoryAllJobsReviews.getData().then(function (data) {
        $scope.reviews = data.data;
        console.log($scope.reviews);


    })*/

    idx = $window.sessionStorage.getItem("CompanyID");
    httpFactoryAllJobsCompanyReviews.redditGet(idx)
        .then(function (response) {
            $rootScope.reviews = response.data;
            console.log($rootScope.reviews);
        });


    $scope.addReplyReviewGetData = function (id) {
        httpFactoryJobReviewID.redditGet(id)
            .then(function (response) {
                $rootScope.jobrev = response;
                console.log($rootScope.jobrev);
                $rootScope.ReviewID = response.data[0].ReviewID;
                //$rootScope.JobID = response.data[0].JobID;
                $rootScope.CompanyName = response.data[0].CompanyName;
            })
    };

    $scope.deleteReview = function (id, name) {
        if ($window.confirm("Please confirm delete Review for: " + name)) {
            $scope.status = "You clicked YES.";

            httpFactoryDeleteReviewID.redditGet(id)
                .then(function (response) {
                    $rootScope.message = 'Deleted Review! Refreshing jobs list.';
                    for (var i = 0; i < $scope.reviews.length; i++) {
                        var t = $scope.reviews[i];
                        if (t.ReviewID === id) {
                            $scope.reviews.splice(i, 1);
                            break;
                        }
                    }

                }, function (error) {
                    $rootScope.message = 'Unable to delete job: ' + error.message;
                });
        } else {
            $scope.Message = "You clicked NO.";
        }

    };

});


app.service("httpFactoryAllJobsReviews", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/JobsReviews");
    }

});


app.factory('httpFactoryAllJobsCompanyReviews', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/JobsCompanyReviews/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/JobsCompanyReviews/')
        }

    }
});


app.factory('httpFactoryJobReviewID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/JobsReviews2/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/JobsReviews2/')
        }

    }
});

app.controller('saveReply', function ($scope, $rootScope, $http) {
    $scope.submit = function (isValid) {

        $scope.url = 'http://localhost:59424/api/ReplyReviews/';
        if (isValid) {

            try {
                var res = $http.post('http://localhost:59424/api/ReplyReviews/',
                    {
                        ReviewID: $rootScope.ReviewID,
                        Comments: $scope.review.Reply,
                        DateT: new Date()
                    });

                //res.success(function (data, status, headers, config) { 
                alert("Successfully Saved", $('#myModal').modal('hide'));
                $scope.review.Reply = "";
                //$scope.message = data;
                /*});
                res.error(function (data, status, headers, config) {
                    alert("failure message: " + JSON.stringify({ data: data }));
                });*/
            }

            catch (err) {
                alert("Error....." + err);
                console.log(err);
            }
        }

        else {

            alert('Form is not valid');
        }
    };


});

app.controller('MyController', function MyController($scope, $rootScope, $http, $window) {

    //the image
    $rootScope.uploadme;


    $scope.uploadImage = function () {

        var fd = new FormData();
        var imgBlob = dataURItoBlob($scope.uploadme);

        $window.sessionStorage.setItem("img", $scope.uploadme);
        fd.append('file', imgBlob);


                /*$http.post('http://localhost:59424/'+
                'imageURL',
                fd, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined


                    }
                })*/

            /*Upload.upload({
                url: 'C:\FILES',
                file: $files,
            }).progress(function (e) {
            }).then(function (data, status, headers, config) {
                // file is uploaded successfully
                console.log(data);
            }) */


               /* .success(function (response) {
                    console.log('success', response);
                })
                .error(function (response) {
                    console.log('error', response);
                })*/;
    }


    //you need this function to convert the dataURI
    function dataURItoBlob(dataURI) {
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    }



});


//your directive
app.directive("fileread", [
    function () {
        return {
            scope: {
                fileread: "="
            },
            link: function (scope, element, attributes) {
                element.bind("change", function (changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.fileread = loadEvent.target.result;
                        });
                    }
                    reader.readAsDataURL(changeEvent.target.files[0]);
                });
            }
        }
    }
]);

app.controller('saveCompanyLogin', ['$scope', '$http', '$window', function ($scope, $http, $window, success, $location) {

    $scope.submit = function (isValid) {
        $scope.url = 'http://localhost:59424/api/Companies/';
        if (isValid) {

            try {
                var res = $http.post('http://localhost:59424/api/Companies/',
                    {
                        CompanyName: $scope.company.CompanyName,
                        Email: $scope.company.CompanyEmail,
                        Password: $scope.company.CompanyPassword
                    });

                //alert("successfully registered");
                //$window.location.href = 'successfullyRegistered.html';
            }

            catch (err) {
                alert("Error...");
                console.log(err);
            }
        }

        else {

            alert('Form is not valid');
        }
    };
}]);




app.controller('CompanyLogin', function ($scope, $rootScope, $http, httpLoginCompany, USER_ROLES, $window) {
    $scope.submit = function (isValid) {
        $scope.company.Email;
        $scope.company.Password;

        $scope.Comp = {
            Email: $scope.company.Email,
            Password: $scope.company.Password
        };

        $scope.Comp2 = {
            data: [
                { Email: $scope.company.Email, Password: $scope.company.Password },
            ]
        };

        httpLoginCompany.redditGet($scope.Comp2)
            .then(function (response) {
                try {
                    if (response.data[0].CompanyID != null) {
                        $rootScope.company = response;

                        $rootScope.CompanyIDx = response.data[0].CompanyID;
                        $rootScope.CompanyName = response.data[0].CompanyName;
                        $rootScope.Address = response.data[0].Address;
                        $rootScope.City = response.data[0].City;
                        $rootScope.State = response.data[0].State;
                        $rootScope.Country = response.data[0].Country;
                        $rootScope.Phone = response.data[0].Phone;
                        $rootScope.Website = response.data[0].Website;
                        $rootScope.CompanyType = response.data[0].CompanyType;
                        $rootScope.CompanySector = response.data[0].CompanySector;
                        $rootScope.CompanyContactName = response.data[0].CompanyContactName;
                        $rootScope.Email = response.data[0].Email;

                        $rootScope.credentials = {
                            companyId: response.data[0].CompanyID,
                            username: response.data[0].CompanyName
                        };


                        loginID = response.data[0].CompanyID;
                        this.CompanyName = response.data[0].CompanyName;

                        $window.sessionStorage.setItem("CompanyID", response.data[0].CompanyID);
                        $window.sessionStorage.setItem("CompanyName", response.data[0].CompanyName);
                        $scope.IDC = $window.sessionStorage.getItem("CompanyID");
                        $scope.NAMEX = $window.sessionStorage.getItem("CompanyName");
                        //alert('Session' + $window.sessionStorage.getItem("CompanyID") + '  ' + $window.sessionStorage.getItem("CompanyName"));

                        $window.location.href = 'index.html';
                    }

                    else {
                        alert('No user found');
                    }
                }
                catch
                {
                    alert('No user found');
                }


            })

    }
});


app.factory('httpLoginCompany', function ($http) {

    return {
        redditGet: function (searchValue) {
            return $http.post('http://localhost:59424/api/Login/',
                {
                    Email: searchValue.data[0].Email,
                    Password: searchValue.data[0].Password
                });
        }
    };


    //return {
    //    redditGet: redditGet
    //}
    //function redditGet(searchValue) {
    //    if (searchValue !== undefined && searchValue !== "") {
    //        return $http.get('http://localhost:59424/api/Companies/' + searchValue)
    //    }

    //    else {
    //        return $http.get('http://localhost:59424/api/Companies/')
    //    }

    //}

});


app.constant('USER_ROLES', {
    all: '*',
    admin: 'admin',
    editor: 'editor',
    guest: 'guest'
}).constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
})



app.controller("piesex", function ($scope, $http, httpFactoryAll, httpGetcompanylist) {



    $scope.compare1 = '';
    $scope.compare2 = "";
    httpGetcompanylist.getData().then(function (d) {

        //$scope.complist = d.data;


    })

    $scope.fillTextbox = function (string) {
        $scope.country = string;
        $scope.filterCountry = null;
    }

    $scope.complete = function (string) {

        var output = [];
        angular.forEach($scope.complist, function (country) {
            if (country.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                output.push(country);
            }
        });
        $scope.filterCountry = output;
    }
    $scope.fillTextbox2 = function (string) {
        $scope.country2 = string;
        $scope.filterCountry2 = null;
    }

    $scope.complete2 = function (string) {
        var output = [];
        angular.forEach($scope.complist, function (country2) {
            if (country2.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                output.push(country2);
            }
        });
        $scope.filterCountry2 = output;
    }


    httpFactoryAll.getData().then(function (d) {

        //$scope.ssd = httpFactoryAll.getData()


        $scope.op = ["JobLocation", "JobReview", "JobByCompany"];

        console.log(($scope.data));
    })



    $scope.ccompare = function () {
        var parameters = {
            select1: $scope.country

        };
        var config = {
            params: parameters
        };
        var parameters2 = {
            select1: $scope.country2

        };
        var config2 = {
            params: parameters2
        };
        $http.get("http://localhost:59424/second/Charts/Getcompare1", config).then(function (d) {
            $scope.la1 = d.data;

        })
        $http.get("http://localhost:59424/second/Charts/Getcompare1", config2).then(function (dd) {
            $scope.la2 = dd.data;

        })
        $scope.labels = [$scope.country, $scope.country2];

        $scope.data = [$scope.la1, $scope.la2];
        console.log(($scope.data));
        console.log(($scope.labels));

    }


    $scope.Getlabel1 = function () {
        var parameters = {
            select: $scope.selectedOption

        };
        var config = {
            params: parameters
        };


        $http.get("http://localhost:59424/second/Charts/Getlabel1", config).then(function (d) {
            $scope.label1 = d.data;
            $scope.complist = d.data;

        })
        $http.get("http://localhost:59424/second/Charts/Getdata1", config).then(function (dd) {
            $scope.data1 = dd.data;

        })

        console.log(($scope.data1));

    }

    $scope.options = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
})

app.controller("barjob", function ($scope, $http, httpJobbymonth) {

    $scope.labels = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    httpJobbymonth.getData().then(function (d) {

        //$scope.ssd = httpFactoryAll.getData()

        $scope.data = d.data;

        $scope.series = ['Series A'];

        console.log(($scope.data));
    })

    $scope.options = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
})

app.controller("clicks", function ($scope, $http, httpGetcompanyname, httpGetcompanyclicks) {

    httpGetcompanyname.getData().then(function (d) {
        $scope.label3 = d.data;
        console.log(d.data);
    })
    httpGetcompanyclicks.getData().then(function (d) {
        $scope.data3 = d.data;
        console.log(d.data);

    })
    $scope.options = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }

})

app.service("httpGetcompanyname", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/second/Charts/Getcompanyname");
    }

});

app.service("httpGetcompanyclicks", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/second/Charts/Getcompanyclicks");
    }

});
app.service("httpGetcompanylist", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/second/Charts/Getcompanylist");
    }

});
app.service("httpFactoryAll", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/second/Charts/Getmale");
    }

});
app.service("httpJobbymonth", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/second/Charts/Getjobsbymonth");
    }

});

app.service("httpGetint", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/second/Charts/Gettest");
    }

});
//app.service("httpGetlabel1", function ($http,$scope) {
//    this.getData = function (s) {
//        return $http.get("http://localhost:59424/second/Charts/Getlabel1?select="+$scope.selectedOption);
//    }

//});
//app.service("httpGetdata1", function ($http) {
//    this.getData = function (s) {
//        return $http.get("http://localhost:59424/second/Charts/Getdata1?select=" + $scope.selectedOption);

//    }

//});


