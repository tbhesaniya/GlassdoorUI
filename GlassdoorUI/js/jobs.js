﻿var app = angular.module('myApp', ['ngRoute']);

app.config(function ($routeProvider, $locationProvider) {

    try {
        $locationProvider.hashPrefix('');
        $routeProvider

            .when('/jobs', {
                templateUrl: 'Jobs.html',
                controller: 'HomeController'
            })

            .when('/dashboard', {
                templateUrl: 'views/dashboardMain.html',
                controller: 'HomeController'
            })

            .when('/createJob', {
                templateUrl: 'createJob.html',
                controller: 'HomeController'
            })

            .when('/EditCompany', {
                templateUrl: 'editCompany.html',
                controller: 'HomeController'
            })



            .otherwise({ redirectTo: '/dashboard' });
    }
    catch (err) {
        alert(err);
    }

});	

app.controller('HomeController', function ($scope) {
    $scope.message = 'Hello from HomeController';
});

app.directive("menuDir", function () {

    var dir = {};
    dir.templateUrl = 'views/menubar.html'
    return dir;
}
);

app.directive("menuDir2", function () {

    var dir = {};
    dir.templateUrl = 'menubar.html'
    return dir;
}
);

app.service("httpFactoryAll", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Jobs");
    }

});

app.service("httpCompanyTypes", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/CompanyTypes");
    }

});

app.service("httpCompanyStatus", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/CompanyStatus");
    }

});


app.service("httpCountries", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Countries");
    }

});

app.controller('ListCountriesController', function ($scope, $rootScope, $http, httpCountries, httpStateID) {

    httpCountries.getData().then(function (data) {
        
        $scope.country = data.data;
        console.log($scope.country);

    })

    $scope.viewState = function (id) {
        httpStateID.redditGet(id)
            .then(function (data) {
                $rootScope.state = data.data;
                console.log($scope.state); 
              
            })
    }
});



app.controller('ListJobsController', function ($scope, $rootScope, $http, httpFactoryAll, httpFactoryJobID, $window, httpFactoryDeleteJobID, httpStateID) {
    httpFactoryAll.getData().then(function (data) {
        $scope.jobs = data.data;
        console.log($scope.jobs); 

        id = data.data.CountryID;
        
        z = 0
        httpStateID.redditGet(id)
            .then(function (dataS) {
                $rootScope.state = dataS.data;
                console.log($scope.state);
            })

        //$scope.jobs.StateID = data.data[z].StateID; 
    })

    $scope.viewJob = function (id) {
        httpFactoryJobID.redditGet(id)
            .then(function (response) {
                $rootScope.job = response;

                $rootScope.JobID = response.data.JobID;
                $rootScope.JobName = response.data[0].JobName;
                $rootScope.JobDescription = response.data[0].JobDescription;
                $rootScope.Company = response.data[0].Company;
                $rootScope.DateCreated = response.data[0].DateCreated;
                $rootScope.Address = response.data[0].Address;
                $rootScope.City = response.data[0].City;
                $rootScope.CityID = response.data[0].CityID;
                $rootScope.State = response.data[0].State;
                $rootScope.Country = response.data[0].Country;
                $rootScope.CountryID = response.data[0].CountryID;
                console.log($rootScope.job);
            })
    };

    $scope.deleteJob = function (id, name) {
        
        if ($window.confirm("Please confirm delete Job: " + name)) {
            $scope.status = "You clicked YES.";

            httpFactoryDeleteJobID.redditGet(id)
                .then(function (response) {
                    $rootScope.message = 'Deleted Job! Refreshing jobs list.';
                    for (var i = 0; i < $scope.jobs.length; i++) {
                        var t = $scope.jobs[i];
                        if (t.JobID === id) {
                            $scope.jobs.splice(i, 1);
                            break;
                        }
                    }

                }, function (error) {
                    $rootScope.message = 'Unable to delete job: ' + error.message;
                });
        } else {
            $scope.Message = "You clicked NO.";
        }

    };

    $scope.updateJob = function (index) {
        if ($scope.editing !== false) {
            $scope.jobs[$scope.editing] = $scope.newField;
            $scope.editing = false;
            $scope.selectedRow = index;
            $scope.jobs[index].JobName;

            $http.put('http://localhost:59424/api/Jobs/' + $scope.jobs[index].JobID,
                {
                    JobID: $scope.jobs[index].JobID,
                    CompanyID: 1,
                    JobName: $scope.jobs[index].JobName,
                    JobDescription: $scope.jobs[index].JobDescription,
                    City: $scope.jobs[index].City,
                    CountryID: $scope.jobs[index].CountryID,
                    StateID: $scope.jobs[index].StateID,
                    DateCreated: $scope.jobs[index].DateCreated,
                    Address: $scope.jobs[index].Address,
                    DaysSponsered: $scope.jobs[index].DaysSponsered
                });
        }
    };


});

app.controller('ListCompanyTypesController', function ($scope, $rootScope, $http, httpCompanyTypes, httpSectorID) {
    httpCompanyTypes.getData().then(function (data) {
        $scope.ct = data.data;
        console.log($scope.ct);

    })

    $scope.viewCompanySector = function (id) {
        httpSectorID.redditGet(id)
            .then(function (data) {
                $rootScope.sector = data.data;
                console.log($scope.sector);

            })
    } 
    
});

app.controller('ListCompanyStatusController', function ($scope, $rootScope, $http, httpCompanyStatus) {
    httpCompanyStatus.getData().then(function (data) {
        $scope.st = data.data;
        console.log($scope.st);

    })
});

app.factory('httpFactoryJobID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/Jobs/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/Jobs/')
        }

    }
});

app.factory('httpStateID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/States/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/States/')
        }

    }
});

app.factory('httpSectorID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/CompanySectors/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/CompanySectors/')
        }

    }
});



app.factory('httpFactoryCompanyID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/Companies/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/Companies/')
        }

    }
});

app.controller('CompanyDataController', function ($scope, $rootScope, $http, httpCountries, httpFactoryCompanyID, httpStateID, httpSectorID) {
    
    httpCountries.getData().then(function (data) {
        id = 1;
        httpFactoryCompanyID.redditGet(id)
            .then(function (data) {
                $rootScope.company = data.data;
                console.log($scope.company);
                $rootScope.company.CompanyID = data.data[0].CompanyID;
                $rootScope.company.CompanyName = data.data[0].CompanyName;
                $rootScope.company.Address = data.data[0].Address;
                $rootScope.company.City = data.data[0].City;
                $rootScope.company.Phone = data.data[0].Phone;
                $rootScope.company.Website = data.data[0].Website;
                $rootScope.company.CompanyContactName = data.data[0].CompanyContactName;
                $rootScope.company.Username = data.data[0].Username;
                $rootScope.company.Password = data.data[0].Password;
                $rootScope.company.Email = data.data[0].Email;
                $rootScope.company.SelectedCountry = data.data[0].CountryID;
                $rootScope.company.CompanyDescription = data.data[0].Description;
                $rootScope.company.SelectedCompanyType = data.data[0].CompanyTypeID;
                $rootScope.company.SelectedStatusType = data.data[0].CompanyStatusID;

                id = data.data[0].CountryID;
                id2 = data.data[0].CompanyTypeID;

                if (id != null)
                {
                    httpStateID.redditGet(id)
                        .then(function (data2) {
                            $rootScope.state = data2.data;
                            console.log($scope.state);
                        })

                    $rootScope.company.SelectedState = data.data[0].StateID;
                }

                if (id2 != null)
                {
                    httpSectorID.redditGet(id2)
                        .then(function (data3) {
                            $rootScope.sector = data3.data;
                            console.log($scope.sector);

                        })

                    $rootScope.company.SelectedSector = data.data[0].CompanySectorID;
                }
                
            })
    })
});


app.factory('httpFactoryCompanyID', function ($http) {
    
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            
            return $http.get('http://localhost:59424/api/Companies/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/Companies/')
        }

    }
});


app.controller('UploadController', function ($scope, fileReader) {
    $scope.imageSrc = "";

    $scope.$on("fileProgress", function (e, progress) {
        $scope.progress = progress.loaded / progress.total;
    });
});




app.directive("ngFileSelect", function (fileReader, $timeout) {
    return {
        scope: {
            ngModel: '='
        },
        link: function ($scope, el) {
            function getFile(file) {
                fileReader.readAsDataUrl(file, $scope)
                    .then(function (result) {
                        $timeout(function () {
                            $scope.ngModel = result;
                        });
                    });
            }

            el.bind("change", function (e) {
                var file = (e.srcElement || e.target).files[0];
                getFile(file);
            });
        }
    };
});

app.factory("fileReader", function ($q, $log) {
    var onLoad = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.resolve(reader.result);
            });
        };
    };

    var onError = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.reject(reader.result);
            });
        };
    };

    var onProgress = function (reader, scope) {
        return function (event) {
            scope.$broadcast("fileProgress", {
                total: event.total,
                loaded: event.loaded
            });
        };
    };

    var getReader = function (deferred, scope) {
        var reader = new FileReader();
        reader.onload = onLoad(reader, deferred, scope);
        reader.onerror = onError(reader, deferred, scope);
        reader.onprogress = onProgress(reader, scope);
        return reader;
    };

    var readAsDataURL = function (file, scope) {
        var deferred = $q.defer();

        var reader = getReader(deferred, scope);
        reader.readAsDataURL(file);

        return deferred.promise;
    };

    return {
        readAsDataUrl: readAsDataURL
    };
});




app.controller('updateCompany',  function ($scope, $rootScope, $http) {
    $scope.submit = function (isValid) {

        $scope.url = 'http://localhost:59424/api/Companies/';
        if (isValid) {

            try {
                var res = $http.put('http://localhost:59424/api/Companies/' + $rootScope.company.CompanyID,
                    {
                        CompanyID: $rootScope.company.CompanyID,
                        CompanyName: $rootScope.company.CompanyName ,
                        Address: $rootScope.company.Address ,
                        City: $rootScope.company.City ,
                        Phone: $rootScope.company.Phone,
                        Website: $rootScope.company.Website ,
                        CompanyContactName: $rootScope.company.CompanyContactName ,
                        Username: $rootScope.company.Username ,
                        Password: $rootScope.company.Password ,
                        Email: $rootScope.company.Email ,
                        CountryID: $rootScope.company.SelectedCountry ,
                        Description: $rootScope.company.CompanyDescription ,
                        CompanyTypeID: $rootScope.company.SelectedCompanyType ,
                        CompanyStatusID: $rootScope.company.SelectedStatusType ,
                        StateID: $rootScope.company.SelectedState ,
                        CompanySectorID: $rootScope.company.SelectedSector

                    });

                //res.success(function (data, status, headers, config) {
                    alert("Successfully Updated ");
                    //$scope.message = data;
                /*});
                res.error(function (data, status, headers, config) {
                    alert("failure message: " + JSON.stringify({ data: data }));
                });*/
            }

            catch (err) {
                alert("Error....."+err);
                console.log(err);
            }
        }

        else {

            alert('Form is not valid');
        }
    };


});


app.controller('createJob', function ($scope, $rootScope, $http) {
    $scope.submit = function (isValid) {

        $scope.url = 'http://localhost:59424/api/Jobs/';
        if (isValid) {

            try {
                var res = $http.post('http://localhost:59424/api/Jobs/',
                    {
                        JobName: $scope.job.JobName,
                        JobDescription: $scope.job.JobDescription,
                        CompanyID: 1,
                        DateCreated: new Date(),
                        Address: $scope.job.JobAddress,
                        City: $scope.job.City,
                        StateID: $scope.job.SelectedState,
                        CountryID: $scope.job.SelectedCountry, 
                        DaysSponsered: $scope.job.DaysSponsored
                    });

                //res.success(function (data, status, headers, config) {
                alert("Successfully Updated ");

                //Clear Form

                $scope.job.JobName = "";
                $scope.job.JobDescription = "";
                $scope.job.JobAddress = "";
                $scope.job.City = "";
                $scope.job.SelectedState = -1,
                $scope.job.SelectedCountry = -1,
                $scope.job.DaysSponsored = "";


                //$scope.message = data;
                /*});
                res.error(function (data, status, headers, config) {
                    alert("failure message: " + JSON.stringify({ data: data }));
                });*/
            }

            catch (err) {
                alert("Error....." + err);
                console.log(err);
            }
        }

        else {

            alert('Form is not valid');
        }
    };
});

app.factory('httpFactoryDeleteJobID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(id) {
        return $http.delete('http://localhost:59424/api/Jobs/' + id);
    };
});

