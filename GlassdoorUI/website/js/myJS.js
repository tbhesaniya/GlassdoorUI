﻿var app = angular.module('myApp', ['ngRoute']);

app.config(function ($routeProvider, $locationProvider) {
    
    try
    {
        $locationProvider.hashPrefix('');
        $routeProvider

            .when('/home', {
                templateUrl: 'views/homeMain.html',
                controller: 'HomeController'
            })

            .when('/jobs', {
                templateUrl: 'job_search.html',
                controller: 'HomeController'
            })

            .when('/employers', {
                templateUrl: 'employers_list.html',
                controller: 'HomeController'
            })

            .when('/contact', {
                templateUrl: 'contact.html',
                controller: 'HomeController'
            })

            .when('/login', {
                templateUrl: 'contact.html',
                controller: 'loginController'
            })

            .when('/profile', {
                templateUrl: 'profile_employer.html',
                controller: 'ProfileController'
            })



            .otherwise({ redirectTo: '/home' });
    }
    catch(err)
    {
        alert(err);
    }
    
});

app.controller('HomeController', function ($scope) {
    $scope.message = 'Hello from HomeController';
});

app.controller('ProfileController', function ($scope, $rootScope, httpAllJobs, httpFactoryCompanyID, httpAllReviews, $routeParams, httpAllCompanyReviews) {
    $scope.message = 'Hello from ProfileController';

    //xx = $routeParams.id;
    id = $routeParams.id;

    httpFactoryCompanyID.redditGet(id)
        .then(function (response) {
            $scope.company = response;

            $scope.CompanyID = response.data[0].CompanyID;
            $scope.CompanyName = response.data[0].CompanyName;
            $scope.Address = response.data[0].Address;
            $scope.City = response.data[0].City;
            $scope.State = response.data[0].State;
            $scope.Country = response.data[0].Country;

            $scope.Phone = response.data[0].Phone;
            $scope.Website = response.data[0].Website;
            $scope.CompanyType = response.data[0].CompanyType;
            $scope.CompanySector = response.data[0].CompanySector;
            $scope.CompanyContactName = response.data[0].CompanyContactName;
            $scope.Email = response.data[0].Email;
            $scope.Description = response.data[0].Description;
            console.log($scope.company);
        })

   /* httpAllCompanyReviews.getData().then(function (dataReviews) {
        $scope.reviews = dataReviews.data;
        console.log($scope.reviews);
        $scope.TotalReviews = dataReviews.data.length;

    })*/
    httpAllCompanyReviews.redditGet(id)
        .then(function (data) {
            $rootScope.reviews = data.data;
            console.log($rootScope.reviews);

        })

});

app.factory('httpFactoryCompanyID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue !== undefined && searchValue !== "") {
            return $http.get('http://localhost:59424/api/Companies/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/Companies/')
        }

    }
});


app.controller('loginController', function ($scope, $window) {
    $window.location.href = '../login.html';
});


app.directive("menuDir", function () {

    var dir = {};
    dir.templateUrl = 'views/menubar.html'
    return dir;
}
);

app.directive("socialDir", function () {

    var dir = {};
    dir.templateUrl = 'views/social.html'
    return dir;
}
);

app.directive("headerDir", function () {

    var dir = {};
    dir.templateUrl = 'views/header.html'
    return dir;
}
);

app.directive("footerDir", function () {

    var dir = {};
    dir.templateUrl = 'views/footer.html'
    return dir;
}
);

app.directive("jobsDir", function () {

    var dir = {};
    dir.templateUrl = 'views/jobsList.html'
    return dir;
}
);

app.directive("companyDir", function () {

    var dir = {};
    dir.templateUrl = 'views/companyList.html'
    return dir;
}
);

app.service("httpAllJobs", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Jobs");
    }
});

app.service("httpAllCompanies", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Companies");
    }

});

app.service("httpAllReviews", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/JobsReviews");
    }

});

app.factory('httpAllCompanyReviews', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue != undefined && searchValue != "") {

            return $http.get('http://localhost:59424/api/JobsReviews/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/JobsReviews/')
        }

    }
});

app.factory('httpFactoryJobID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue != undefined && searchValue != "") {
            return $http.get('http://localhost:59424/api/Jobs/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/Jobs/')
        }

    }
});
 
app.controller('IndexController', function ($scope, $rootScope, $http, httpAllJobs, httpAllCompanies, httpAllReviews, httpFactoryJobID, httpFactoryCompanyID) {
    httpAllJobs.getData().then(function (dataJobs) {
        $scope.jobs = dataJobs.data;
        console.log($scope.jobs);
        $scope.TotalJobs = dataJobs.data.length;
    })

    httpAllCompanies.getData().then(function (dataCompanies) {
        $scope.company = dataCompanies.data;
        console.log($scope.company);
        $scope.TotalCompanies = dataCompanies.data.length;
    })

    httpAllReviews.getData().then(function (dataReviews) {
        $scope.reviewsIndex = dataReviews.data;
        console.log($scope.reviewsIndex); 
        $scope.TotalReviews = dataReviews.data.length;
    })

    /*$scope.AddReviewGetData = function (id) {
        
        httpFactoryJobID.redditGet(id)
            .then(function (response) {
                $rootScope.job = response;
                console.log($rootScope.job);
                $rootScope.JobID = response.data[0].JobID;
                $rootScope.Company = response.data[0].Company;
                $rootScope.JobName = response.data[0].JobName;
            }) 
    };*/
    $scope.AddReviewGetCompanyData = function (id) {
        
        httpFactoryCompanyID.redditGet(id) 
            .then(function (data) {
                $rootScope.CompanyName = data.data[0].CompanyName;
                $rootScope.CompanyID = data.data[0].CompanyID;
            }
        )
    }



    $scope.viewJob = function (id) {
        
        httpFactoryJobID.redditGet(id)
            .then(function (response) {
                $rootScope.job = response;

                $rootScope.JobID = response.data[0].JobID;
                $rootScope.JobName = response.data[0].JobName;
                $rootScope.JobDescription = response.data[0].JobDescription;
                $rootScope.Company = response.data[0].Company;
                $rootScope.DateCreated2 = response.data[0].DateCreated2;
                $rootScope.Address = response.data[0].Address;
                $rootScope.City = response.data[0].City;
                $rootScope.State = response.data[0].State;
                $rootScope.StateID = response.data[0].StateID;
                $rootScope.Country = response.data[0].Country;
                $rootScope.CountryID = response.data[0].CountryID;
                $rootScope.vDaysSponsered = response.data[0].DaysSponsered;
                $rootScope.CompanyID = response.data[0].CompanyID;
                $rootScope.cl = response.data[0].Clicks + 1;

                console.log($rootScope.job);
                idC = response.data[0].CompanyID;
                //idx = $window.sessionStorage.getItem("CompanyID");
                $http.put('http://localhost:59424/api/djob/' + id,
                    {
                        JobID: id,
                        JobName: $rootScope.JobName,
                        JobDescription: $rootScope.JobDescription,
                        CompanyID: idC,
                        DateCreated: $rootScope.DateCreated2,
                        Address: $rootScope.Address,
                        City: $rootScope.City,
                        StateID: $rootScope.StateID,
                        CountryID: $rootScope.CountryID,
                        DaysSponsered: $rootScope.vDaysSponsered,
                        Clicks: $rootScope.cl
                    })


            })

        idx = $window.sessionStorage.getItem("CompanyID");
        httpFactoryJobCompanyID.redditGet(idx)
            .then(function (response) {
                $rootScope.jobsX = response.data;
                console.log($rootScope.jobsX);
            })
    };
            
   
    
});

app.controller('saveReview', function ($scope, $rootScope, $http) {
    $scope.submit = function (isValid) {
        $scope.url = 'http://localhost:59424/api/JobsReviews/';
        if (isValid) {

            try {
                var res = $http.post('http://localhost:59424/api/JobsReviews/',
                    {
                        DateT: new Date(),
                        Comments: $scope.review.Comments,
                        Name: $scope.review.Name,
                        Profession: $scope.review.Profession,
                        CompanyID: $rootScope.CompanyID 
                    });

                //res.success(function (data, status, headers, config) { 
                alert("Successfully Saved", $('#myModal').modal('hide'));

                $scope.review.Comments = "";
                $scope.review.Name = "";
                $scope.review.Profession = "";
                //$scope.message = data;
                /*});
                res.error(function (data, status, headers, config) {
                    alert("failure message: " + JSON.stringify({ data: data }));
                });*/
            }

            catch (err) {
                alert("Error....." + err);
                console.log(err);
            }
        }

        else {

            alert('Form is not valid');
        }
    };


    $scope.close = function () {
        $modalInstanceProvider.dismiss('cancel');
    };
});

