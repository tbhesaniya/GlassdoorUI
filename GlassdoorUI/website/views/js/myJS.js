﻿var app = angular.module('myApp', ['ngRoute']);

app.config(function ($routeProvider, $locationProvider) {

    try
    {
        $locationProvider.hashPrefix('');
        $routeProvider

            .when('/home', {
                templateUrl: 'views/homeMain.html',
                controller: 'HomeController'
            })



            .otherwise({ redirectTo: '/home' });
    }
    catch(err)
    {
        alert(err);
    }
    
});

app.controller('HomeController', function ($scope) {
    $scope.message = 'Hello from HomeController';
});


app.directive("menuDir", function () {

    var dir = {};
    dir.templateUrl = 'views/menubar.html'
    return dir;
}
);

app.directive("socialDir", function () {

    var dir = {};
    dir.templateUrl = 'views/social.html'
    return dir;
}
);

app.directive("headerDir", function () {

    var dir = {};
    dir.templateUrl = 'views/header.html'
    return dir;
}
);

app.directive("footerDir", function () {

    var dir = {};
    dir.templateUrl = 'views/footer.html'
    return dir;
}
);

app.directive("jobsDir", function () {

    var dir = {};
    dir.templateUrl = 'views/jobsList.html'
    return dir;
}
);

app.directive("companyDir", function () {

    var dir = {};
    dir.templateUrl = 'views/companyList.html'
    return dir;
}
);

app.service("httpAllJobs", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Jobs");
    }
});

app.service("httpAllCompanies", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/Companies");
    }

});

app.service("httpAllReviews", function ($http) {
    this.getData = function () {
        return $http.get("http://localhost:59424/api/JobsReviews");
    }

});

app.factory('httpFactoryJobID', function ($http) {
    return {
        redditGet: redditGet
    }
    function redditGet(searchValue) {
        if (searchValue != undefined && searchValue != "") {
            return $http.get('http://localhost:59424/api/Jobs/' + String(searchValue))
        }

        else {
            return $http.get('http://localhost:59424/api/Jobs/')
        }

    }
});
 
app.controller('IndexController', function ($scope, $rootScope, $http, httpAllJobs, httpAllCompanies, httpAllReviews, httpFactoryJobID) {
    httpAllJobs.getData().then(function (dataJobs) {
        $scope.jobs = dataJobs.data;
        console.log($scope.jobs);
        $scope.TotalJobs = dataJobs.data.length;
    })

    httpAllCompanies.getData().then(function (dataCompanies) {
        $scope.company = dataCompanies.data;
        console.log($scope.company);
        $scope.TotalCompanies = dataCompanies.data.length;
    })

    httpAllReviews.getData().then(function (dataReviews) {
        $scope.reviews = dataReviews.data;
        console.log($scope.reviews);
        //$scope.TotalCompanies = dataCompanies.data.length;
    })

    $scope.AddReviewGetData = function (id) {
        
        httpFactoryJobID.redditGet(id)
            .then(function (response) {
                $rootScope.job = response;
                console.log($rootScope.job);
                $rootScope.JobID = response.data[0].JobID;
                $rootScope.Company = response.data[0].Company;
                $rootScope.JobName = response.data[0].JobName;
            })
    };
    
});

app.controller('saveReview', function ($scope, $rootScope, $http) {
    $scope.submit = function (isValid) {

        $scope.url = 'http://localhost:59424/api/JobsReviews/';
        if (isValid) {

            try {
                var res = $http.post('http://localhost:59424/api/JobsReviews/',
                    {
                        DateT: new Date(),
                        Comments: $scope.review.Comments,
                        Name: $scope.review.Name,
                        Profession: $scope.review.Profession,
                        JobID: $rootScope.JobID 
                    });

                //res.success(function (data, status, headers, config) { 
                alert("Successfully Saved");
                //$scope.message = data;
                /*});
                res.error(function (data, status, headers, config) {
                    alert("failure message: " + JSON.stringify({ data: data }));
                });*/
            }

            catch (err) {
                alert("Error....." + err);
                console.log(err);
            }
        }

        else {

            alert('Form is not valid');
        }
    };


});

